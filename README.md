Pour créer un conteneur Docker capable de lancer des tests JMeter via GitLab CI : 

    - dans le fichier .env, renesigner la variable REGISTRATION_TOKEN avec le registration token du projet Gitlab auquel le runner va être associer (la valeur du token se trouve dans  Settings -> CI/CD -> Runners -> Project runners)

    - lancer Docker Desktop en mode administrateur

    - ouvrir un terminal en mode administrateur et se déplacer dans le répertoire contenant le fichier docker-compose.yml

    - exécuter la commande suivante : docker-compose up -d

    - le runner devrait apparaître dans la liste des runners du projet (Settings -> CI/CD -> Runners -> Project runners)
